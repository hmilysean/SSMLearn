package com.rcsit.controller.utils;

import org.apache.cxf.jaxrs.client.JAXRSClientFactory;
import org.codehaus.jackson.jaxrs.JacksonJsonProvider;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by hmily on 2017/9/13.
 */
public class ClientProxy<T> {

    public static <T> T create(Class<T> clazz){
        String baseAddress = "http://localhost:8080/";
        //平台API服务使用的JSON序列化提供类
        List<Object> providers = new ArrayList<>();
        providers.add(new JacksonJsonProvider());
        //API会话检查的客户端过滤器
        //providers.add(new SessionClientRequestFilter());
        return JAXRSClientFactory.create(baseAddress, clazz, providers);
    }
}
