package com.rcsit.controller.baf;


import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.stereotype.Controller;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author 梅景飞
 * @since 2017-11-03
 */
@Controller
@RequestMapping("/bankType")
public class BankTypeController {
	
}
