import com.rcsit.dto.baf.BankTypeVo;
import com.rcsit.entity.baf.BankType;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import java.util.ArrayList;
import java.util.List;

/**
 * DESC:
 * USER:hmily
 * DATE:2017/11/3
 * TIME:14:09
 */
public class JaxBTest {
    public static void main(String[] args) throws JAXBException {
        BankType bankType = new BankType();
        bankType.setQbanks("111");
        bankType.setQbanksName("123");
        bankType.setCode("qw23e");

        List<BankType> bankTypes =new ArrayList<>();
        bankTypes.add(bankType);

        BankTypeVo bankTypeVo = new BankTypeVo();
        bankTypeVo.setDataList(bankTypes);

        JAXBContext context = JAXBContext.newInstance(BankTypeVo.class);

        Marshaller marshaller = context.createMarshaller();
        Unmarshaller unmarshaller = context.createUnmarshaller();

        marshaller.marshal(bankTypeVo, System.out);
        System.out.println();

        /*String xml = "<boy><name>David</name></boy>";
        Boy boy2 = (Boy) unmarshaller.unmarshal(new StringReader(xml));
        System.out.println(boy2.name);*/
    }
}
