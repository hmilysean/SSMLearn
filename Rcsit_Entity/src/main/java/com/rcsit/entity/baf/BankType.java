package com.rcsit.entity.baf;

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;

import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * 
 * </p>
 *
 * @author 梅景飞
 * @since 2017-11-03
 */
@TableName("BANK_TYPE")
@XmlRootElement
public class BankType extends Model<BankType> {

    private static final long serialVersionUID = 1L;

    @TableId("ID")
	private String id;
	@TableField("CREATE_DATE")
	private Date createDate;
	@TableField("CREATOR")
	private String creator;
	@TableField("LAST_EDIT_DATE")
	private Date lastEditDate;
	@TableField("EDITOR")
	private String editor;
	@TableField("INPUT_TYPE")
	private String inputType;
	@TableField("IS_FREEZE")
	private String isFreeze;
	@TableField("RANK")
	private String rank;
	@TableField("TENANT_CODE")
	private String tenantCode;
	@TableField("CODE")
	private String code;
	@TableField("CREATOR_ACCOUNT")
	private String creatorAccount;
	@TableField("CREATOR_ID")
	private String creatorId;
	@TableField("CREATOR_NAME")
	private String creatorName;
	@TableField("FROZEN")
	private String frozen;
	@TableField("LAST_EDITOR__ACCOUNT")
	private String lastEditorAccount;
	@TableField("LAST_EDITOR__ID")
	private String lastEditorId;
	@TableField("LAST_EDITOR_NAME")
	private String lastEditorName;
	@TableField("NAME")
	private String name;


	@TableField("QBANKS")
	private String qbanks;
	@TableField("QREMARK1")
	private String qremark1;
	@TableField("QREMARK2")
	private String qremark2;
	@TableField("QREMARK3")
	private String qremark3;
	@TableField("QREMARK4")
	private String qremark4;
	@TableField("QREMARK5")
	private String qremark5;
	@TableField("QAUTO_UPDATE")
	private String qautoUpdate;
	@TableField("QBANKS_NAME_EN")
	private String qbanksNameEn;
	@TableField("QBANKS_NAME")
	private String qbanksName;
	@TableField("QBANKST")
	private String qbankst;
	@TableField("QBANKTXT")
	private String qbanktxt;
	@TableField("QIS_MULTI")
	private String qisMulti;
	@TableField("QMATCH_BANKS")
	private String qmatchBanks;
	@TableField("QMULTI_BANKS")
	private String qmultiBanks;


	@TableField("QSCODE")
	private String qscode;

	@TableField("QSNAME")
	private String qsname;


	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public String getCreator() {
		return creator;
	}

	public void setCreator(String creator) {
		this.creator = creator;
	}

	public Date getLastEditDate() {
		return lastEditDate;
	}

	public void setLastEditDate(Date lastEditDate) {
		this.lastEditDate = lastEditDate;
	}

	public String getEditor() {
		return editor;
	}

	public void setEditor(String editor) {
		this.editor = editor;
	}

	public String getInputType() {
		return inputType;
	}

	public void setInputType(String inputType) {
		this.inputType = inputType;
	}

	public String getIsFreeze() {
		return isFreeze;
	}

	public void setIsFreeze(String isFreeze) {
		this.isFreeze = isFreeze;
	}

	public String getRank() {
		return rank;
	}

	public void setRank(String rank) {
		this.rank = rank;
	}

	public String getTenantCode() {
		return tenantCode;
	}

	public void setTenantCode(String tenantCode) {
		this.tenantCode = tenantCode;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getCreatorAccount() {
		return creatorAccount;
	}

	public void setCreatorAccount(String creatorAccount) {
		this.creatorAccount = creatorAccount;
	}

	public String getCreatorId() {
		return creatorId;
	}

	public void setCreatorId(String creatorId) {
		this.creatorId = creatorId;
	}

	public String getCreatorName() {
		return creatorName;
	}

	public void setCreatorName(String creatorName) {
		this.creatorName = creatorName;
	}

	public String getFrozen() {
		return frozen;
	}

	public void setFrozen(String frozen) {
		this.frozen = frozen;
	}

	public String getLastEditorAccount() {
		return lastEditorAccount;
	}

	public void setLastEditorAccount(String lastEditorAccount) {
		this.lastEditorAccount = lastEditorAccount;
	}

	public String getLastEditorId() {
		return lastEditorId;
	}

	public void setLastEditorId(String lastEditorId) {
		this.lastEditorId = lastEditorId;
	}

	public String getLastEditorName() {
		return lastEditorName;
	}

	public void setLastEditorName(String lastEditorName) {
		this.lastEditorName = lastEditorName;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getQbanks() {
		return qbanks;
	}

	public void setQbanks(String qbanks) {
		this.qbanks = qbanks;
	}

	public String getQremark1() {
		return qremark1;
	}

	public void setQremark1(String qremark1) {
		this.qremark1 = qremark1;
	}

	public String getQremark2() {
		return qremark2;
	}

	public void setQremark2(String qremark2) {
		this.qremark2 = qremark2;
	}

	public String getQremark3() {
		return qremark3;
	}

	public void setQremark3(String qremark3) {
		this.qremark3 = qremark3;
	}

	public String getQremark4() {
		return qremark4;
	}

	public void setQremark4(String qremark4) {
		this.qremark4 = qremark4;
	}

	public String getQremark5() {
		return qremark5;
	}

	public void setQremark5(String qremark5) {
		this.qremark5 = qremark5;
	}

	public String getQautoUpdate() {
		return qautoUpdate;
	}

	public void setQautoUpdate(String qautoUpdate) {
		this.qautoUpdate = qautoUpdate;
	}

	public String getQbanksNameEn() {
		return qbanksNameEn;
	}

	public void setQbanksNameEn(String qbanksNameEn) {
		this.qbanksNameEn = qbanksNameEn;
	}

	public String getQbanksName() {
		return qbanksName;
	}

	public void setQbanksName(String qbanksName) {
		this.qbanksName = qbanksName;
	}

	public String getQbankst() {
		return qbankst;
	}

	public void setQbankst(String qbankst) {
		this.qbankst = qbankst;
	}

	public String getQbanktxt() {
		return qbanktxt;
	}

	public void setQbanktxt(String qbanktxt) {
		this.qbanktxt = qbanktxt;
	}

	public String getQisMulti() {
		return qisMulti;
	}

	public void setQisMulti(String qisMulti) {
		this.qisMulti = qisMulti;
	}

	public String getQmatchBanks() {
		return qmatchBanks;
	}

	public void setQmatchBanks(String qmatchBanks) {
		this.qmatchBanks = qmatchBanks;
	}

	public String getQmultiBanks() {
		return qmultiBanks;
	}

	public void setQmultiBanks(String qmultiBanks) {
		this.qmultiBanks = qmultiBanks;
	}

	public String getQscode() {
		return qscode;
	}

	public void setQscode(String qscode) {
		this.qscode = qscode;
	}

	public String getQsname() {
		return qsname;
	}

	public void setQsname(String qsname) {
		this.qsname = qsname;
	}

	@Override
	protected Serializable pkVal() {
		return this.id;
	}

	@Override
	public String toString() {
		return "BankType{" +
			"id=" + id +
			", createDate=" + createDate +
			", creator=" + creator +
			", lastEditDate=" + lastEditDate +
			", editor=" + editor +
			", inputType=" + inputType +
			", isFreeze=" + isFreeze +
			", rank=" + rank +
			", tenantCode=" + tenantCode +
			", code=" + code +
			", creatorAccount=" + creatorAccount +
			", creatorId=" + creatorId +
			", creatorName=" + creatorName +
			", frozen=" + frozen +
			", lastEditorAccount=" + lastEditorAccount +
			", lastEditorId=" + lastEditorId +
			", lastEditorName=" + lastEditorName +
			", name=" + name +
			", qbanks=" + qbanks +
			", qremark1=" + qremark1 +
			", qremark2=" + qremark2 +
			", qremark3=" + qremark3 +
			", qremark4=" + qremark4 +
			", qremark5=" + qremark5 +
			", qautoUpdate=" + qautoUpdate +
			", qbanksNameEn=" + qbanksNameEn +
			", qbanksName=" + qbanksName +
			", qbankst=" + qbankst +
			", qbanktxt=" + qbanktxt +
			", qisMulti=" + qisMulti +
			", qmatchBanks=" + qmatchBanks +
			", qmultiBanks=" + qmultiBanks +
			", qscode=" + qscode +
			", qsname=" + qsname +
			"}";
	}
}
