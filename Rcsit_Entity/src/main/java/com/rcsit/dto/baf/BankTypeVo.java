package com.rcsit.dto.baf;

import com.baomidou.mybatisplus.activerecord.Model;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

/**
 * DESC:
 * USER:hmily
 * DATE:2017/11/3
 * TIME:14:31
 */
@XmlRootElement(name = "Configures")
public class BankTypeVo<T extends Model> {

    @XmlElement
    List<T> dataList;

    public List<T> getDataList() {
        return dataList;
    }

    public void setDataList(List<T> dataList) {
        this.dataList = dataList;
    }
}
