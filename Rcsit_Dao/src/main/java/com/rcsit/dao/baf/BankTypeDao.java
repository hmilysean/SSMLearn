package com.rcsit.dao.baf;

import com.rcsit.entity.baf.BankType;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
  *  Mapper 接口
 * </p>
 *
 * @author 梅景飞
 * @since 2017-11-03
 */
public interface BankTypeDao extends BaseMapper<BankType> {

}