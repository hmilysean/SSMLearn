package com.rcsit.api.baf;

import com.baomidou.mybatisplus.service.IService;
import com.rcsit.entity.baf.BankType;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.util.Collection;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author 梅景飞
 * @since 2017-11-03
 */
@Path("bankType")
@Consumes({MediaType.APPLICATION_JSON,MediaType.APPLICATION_XML})
@Produces({MediaType.APPLICATION_JSON,MediaType.APPLICATION_XML})
public interface IBankTypeService extends IService<BankType> {

    @GET
    @Path("getBankTypeList")
    Collection<BankType> getBankTypeList();

    @GET
    @Path("findFirst")
    BankType findFirst();
	
}
