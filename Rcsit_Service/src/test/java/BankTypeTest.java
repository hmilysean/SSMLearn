import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.rcsit.api.baf.IBankTypeService;
import com.rcsit.entity.baf.BankType;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import javax.annotation.Resource;

/**
 * DESC:
 * USER:hmily
 * DATE:2017/11/3
 * TIME:11:16
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:spring/spring-content.xml"})
public class BankTypeTest {

    @Resource
    private IBankTypeService iBankTypeService;

    @Test
    public void testBankType(){
        EntityWrapper<BankType> ew = new EntityWrapper<>();
        /*条件语句
        BankType condition = new BankType();
        condition.setQbanks("012");
        ew.setEntity(condition);*/
        //ew 语句
        //ew.where("QBANKS = {0}","012");

    /*    List<BankType> bankTypes = iBankTypeService.selectList(ew);
        bankTypes.forEach(bankType -> {
            System.out.println(bankType.getQbanksName());
        });*/

        Page<BankType> bankTypePage = iBankTypeService.selectPage(new Page<>(2, 4), ew);
        bankTypePage.getRecords().forEach(bankType -> {
            System.out.println(bankType.getQbanksName());
        });
    }
}
