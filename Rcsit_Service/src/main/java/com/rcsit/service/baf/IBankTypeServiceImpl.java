package com.rcsit.service.baf;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.rcsit.api.baf.IBankTypeService;
import com.rcsit.dao.baf.BankTypeDao;
import com.rcsit.entity.baf.BankType;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author 梅景飞
 * @since 2017-11-03
 */
@Service
public class IBankTypeServiceImpl extends ServiceImpl<BankTypeDao, BankType> implements IBankTypeService {

    @Override
    public List<BankType> getBankTypeList() {
        return selectList(null);
    }

    @Override
    public BankType findFirst() {
        return selectOne(null);
    }
}
